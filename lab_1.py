#Adapted from -> https://github.com/dennybritz/reinforcement-learning/blob/master/DP/

import numpy as np
from environments.gridworld import GridworldEnv

def policy_evaluation(env, policy, discount_factor=1.0, theta=0.00001):

    V = np.zeros(env.observation_space.n)
    while True:
        delta = 0
        for state in range(env.observation_space.n):
            v = 0
            for action, action_prob in enumerate(policy[state]):
                for prob, next_state, reward, done in env.P[state][action]:
                    v += action_prob * prob * (reward + discount_factor * V[next_state])
            delta = max(delta, np.abs(v - V[state]))
            V[state] = v
        if delta < theta:
            break
    return np.array(V)

def policy_iteration(env, policy_evaluation_fn=policy_evaluation, discount_factor=1.0):

    def one_step_lookahead(state, V):
        A = np.zeros(env.action_space.n)
        for action in range(env.action_space.n):
            for prob, next_state, reward, done in env.P[state][action]:
                A[action] += prob * (reward + discount_factor * V[next_state])
        return A

    policy = np.ones([env.observation_space.n, env.action_space.n]) / env.action_space.n

    while True:
        V = policy_evaluation_fn(env, policy, discount_factor)
        policy_stable = True
        for s in range(env.observation_space.n):
            chosen_a = np.argmax(policy[s])
            action_values = one_step_lookahead(s, V)
            best_a = np.argmax(action_values)
            if chosen_a != best_a:
                policy_stable = False
            policy[s] = np.eye(env.action_space.n)[best_a]
        if policy_stable:
            return policy, V

def value_iteration(env, theta=0.0001, discount_factor=1.0):

    def one_step_lookahead(state, V):
        A = np.zeros(env.action_space.n)
        for action in range(env.action_space.n):
            for prob, next_state, reward, done in env.P[state][action]:
                A[action] += prob * (reward + discount_factor * V[next_state])
        return A

    V = np.zeros(env.observation_space.n)
    while True:
        delta = 0
        for state in range(env.observation_space.n):
            A = one_step_lookahead(state, V)
            best_action_value = np.max(A)
            delta = max(delta, np.abs(best_action_value - V[state]))
            V[state] = best_action_value
        if delta < theta:
            break

    policy = np.zeros([env.observation_space.n, env.action_space.n])
    for state in range(env.observation_space.n):
        A = one_step_lookahead(state, V)
        best_action = np.argmax(A)
        policy[state, best_action] = 1.0

    return policy, V

def actionsToGrid (env, policy):
    bestActions = np.zeros(env.observation_space.n)
    action = {0:1, 1:2, 2:3, 3:4}
    for i in range (len(policy)):
        for j in range (env.action_space.n):
            if policy[i][j] == 1:
                bestActions[i] = action[j]
    return bestActions

def main():
    dim_1, dim_2 = 5,5
    env = GridworldEnv(shape=[dim_1, dim_2], terminal_states=[24], terminal_reward=0, step_reward=-1)
    state = env.reset()
    print("")
    env.render()
    print("")

    # TODO: generate random policy
    random_policy = np.ones([env.observation_space.n, env.action_space.n]) / env.action_space.n
    #print("obs and act space: ", env.observation_space.n, '  ', env.action_space.n)
    # random policy is a distribution of expressing the random chance 1 of 4 actions will be selected i.e. 25% in every direction
    # therefore for each one of the 25 states there are 4 values describing the policy e.g. action{north:0.25, south:0.25, east:0.25, west:0.25}
    # the policy is described by a 25x4 array, that is the state_space(observation_space) x the action_space

    # TODO: evaluate random policy
    print("*" * 5 + " Policy evaluation " + "*" * 5)
    print("")
    v = policy_evaluation(env,random_policy)
    # TODO: print state value for each state, as grid shape
    v = np.round(v,2)
    print(v.reshape(dim_1,dim_2))
    # Test: Make sure the evaluated policy is what we expected
    expected_v = np.array([-106.81, -104.81, -101.37, -97.62, -95.07,
                           -104.81, -102.25, -97.69, -92.40, -88.52,
                           -101.37, -97.69, -90.74, -81.78, -74.10,
                           -97.62, -92.40, -81.78, -65.89, -47.99,
                           -95.07, -88.52, -74.10, -47.99, 0.0])
    np.testing.assert_array_almost_equal(v, expected_v, decimal=2)

    print("")
    print("*" * 5 + " Policy iteration " + "*" * 5)
    print("")
    # TODO: use  policy improvement to compute optimal policy and state values
    policy, v = policy_iteration(env) # call policy_iteration
    # TODO Print out best action for each state in grid shape
    #best action distribution 25x4
    print("")
    print("Best Actions:")
    print("1:Up, 2:Right, 3:Down, 4:Left")
    print(actionsToGrid(env, policy).reshape(dim_1, dim_2))
    # TODO: print state value for each state, as grid shape
    print("")
    print("State Values")
    print(v.reshape(dim_1, dim_2))
    # Test: Make sure the value function is what we expected
    expected_v = np.array([-8., -7., -6., -5., -4.,
                           -7., -6., -5., -4., -3.,
                           -6., -5., -4., -3., -2.,
                           -5., -4., -3., -2., -1.,
                           -4., -3., -2., -1., 0.])
    np.testing.assert_array_almost_equal(v, expected_v, decimal=1)

    print("")
    print("*" * 5 + " Value iteration " + "*" * 5)
    print("")
    # TODO: use  value iteration to compute optimal policy and state values
    policy, v = value_iteration((env)) # call value_iteration
    # TODO Print out best action for each state in grid shape
    print("")
    print("Best Actions:")
    print ("1:Up, 2:Right, 3:Down, 4:Left")
    print (actionsToGrid(env, policy).reshape(dim_1,dim_2))
    # TODO: print state value for each state, as grid shape
    print("")
    print("State Values")
    print(v.reshape(dim_1, dim_2))
    # Test: Make sure the value function is what we expected
    expected_v = np.array([-8., -7., -6., -5., -4.,
                           -7., -6., -5., -4., -3.,
                           -6., -5., -4., -3., -2.,
                           -5., -4., -3., -2., -1.,
                           -4., -3., -2., -1., 0.])
    np.testing.assert_array_almost_equal(v, expected_v, decimal=1)

if __name__ == "__main__":
    main()
